//
//  APIManager.swift
//  Proto
//
//  Created by Алексей Рябов on 15.09.2018.
//  Copyright © 2018 Zamazra games. All rights reserved.
//

import UIKit

public class APIManager {
    
    private init() { }
    public static let shared: APIManager = APIManager()
    
    
    /// Выполняет GET запрос
    ///
    /// - Parameters:
    ///   - url: URL по которому нужно выполнить зарпос
    ///   - completion: замыкание в которое передается результат работы
    public func load(_ url: String, completion: @escaping (Any?)-> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        URLSession.shared.dataTask(with: URL(string: url)!) { (data, response, error) in
            guard let data = data, error == nil else {
                completion(nil)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                return
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            (completion(try? JSONSerialization.jsonObject(with: data, options:.mutableContainers)) as AnyObject)
                .resume()
        }
    }
    

    /// Запрашивает информацию о зарегистрованном юзере
    ///
    /// - Parameter completion: замыкание в которое передается User
    public func getUser(completion: @escaping (User?)-> Void) {
        guard let token = Credential.sharedInstance.token else {
            completion(nil)
            return
        }
        
        let url = "\(Constants.API.hostname)\("users/self/")\(Constants.API.tokenParameter)\(token)"
        self.load(url) { (json) in
        if let json = (json as? [String: Any])?["data"] as? [String: Any] {
            let user = User(response: json)
            completion(user)
            return
        }
        completion(nil)
    }
}
