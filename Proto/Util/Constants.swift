//
//  Constants.swift
//  Proto
//
//  Created by Алексей Рябов on 08.09.2018.
//  Copyright © 2018 Zamazra games. All rights reserved.
//

import Foundation


/// Хранит необходимый набор констант
public class Constants {
    private init() {}
    
    /// Страницы авторизации
    public static var authUrl: URL {
        get{
            let stringUrl = "https://api.instagram.com/oauth/authorize/?client_id=d3c2f9ad03754bde9cba8a9ce3ea723b&scope=public_content+follower_list+relationships+comments+likes&redirect_uri=https://instagram.com&response_type=token"
            return URL(string: stringUrl)!
        }
    }
    
    
    /// Адресс сервиса
    public struct API {
        static let hostname = "https://api.instagram.com/v1/"
        static let tokenParameter = "?access_token="
    }
    
    
    
}
