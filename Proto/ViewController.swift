//
//  ViewController.swift
//  Proto
//
//  Created by Алексей Рябов on 08.09.2018.
//  Copyright © 2018 Zamazra games. All rights reserved.
//

import UIKit

/// Главный экран приложения.
class ViewController: UIViewController, UserDelagate {
    
    /// Отображает имя пользователя.
    private var _nameLabel: UILabel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self._setup()
        self._getUser()
    }

    /// Получает информацию о зарегистрированном юзере.
    private func _getUser() {
        APIManager.shared.getUser { (user) in
            guard let user = user else {
                self.logout()
                return
            }
            self._nameLabel?.text = user.full_name
        }
    }
    
    func logout() {
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Добавляет необходимые элементы на контроллер.
    private func _setup() {
        let nameLabel = UILabel()
        self.view.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (make) -> Void in
            make.center.equalTo(self.view)
            make.width.height.equalTo(self.view)
        }
        self._nameLabel = nameLabel
    }

    func userCreated(user: User) {
        self._nameLabel?.text = user.full_name
    }
}

